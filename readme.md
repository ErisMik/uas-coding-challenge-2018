# UAS-Coding-Challenge-2018
## Django Advanced Server Challenge (B-SRV-2)

- Please make a simple frontend interface will connect the server to a drone 
- For the sitl drone, see http://python.dronekit.io/develop/sitl_setup.html.
- You may connect it using the dronekit library http://python.dronekit.io/ 
- The frontend should offer a port number and a button, that will connect to the simulator on-click.  If clicked again, it will disconnect from the copter
- Add Django tests and error catching


## Requirements
See `requirements.txt`
```
django==1.11.15
dronekit
dronekit-sitl
```
Can be installed via pip using `pip install -r requirements.txt`


## Running
Run the command `./manage.py runserver` from the root directory and connect to `127.0.0.1:8000/dkconnect/` in your browser