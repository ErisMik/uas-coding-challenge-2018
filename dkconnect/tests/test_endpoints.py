from django.test import TestCase, Client
import dronekit_sitl

sitl_vehicle = None

class EndpointsTestCase(TestCase):
    def setUp(self):
        global sitl_vehicle
        sitl_vehicle = dronekit_sitl.start_default()

    def tearDown(self):
        global sitl_vehicle        
        sitl_vehicle.stop()

    ##################################################################
    ## Tests for /dkconnect

    def test_get_index(self):
        """Tests that a GET returns correct status_code"""
        client = Client()
        response = client.get('/dkconnect/')
        self.assertEqual(response.status_code, 200)

    def test_post_index(self):
        """Tests that a POST returns correct status_code"""
        client = Client()
        response = client.post('/dkconnect/', {'port': 5760})
        self.assertEqual(response.status_code, 405)

    def test_put_index(self):
        """Tests that a PUT returns correct status_code"""
        client = Client()
        response = client.put('/dkconnect/', {'port': 5760})
        self.assertEqual(response.status_code, 405)

    ##################################################################
    ## Tests for /dkconnect/connect

    def test_post_api_connect(self):
        """Tests that a POST returns correct status_code"""
        client = Client()
        response = client.post('/dkconnect/connect/', {'port': 5760})
        self.assertEqual(response.status_code, 200)
    
    def test_get_api_connect(self):
        """Tests that a GET returns correct status_code"""
        client = Client()
        response = client.get('/dkconnect/connect/')
        self.assertEqual(response.status_code, 405)

    def test_put_api_connect(self):
        """Tests that a PUT returns correct status_code"""
        client = Client()
        response = client.put('/dkconnect/connect/', {'port': 5760})
        self.assertEqual(response.status_code, 405)