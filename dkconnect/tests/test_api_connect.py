from django.test import TestCase, Client
import dronekit_sitl

sitl_vehicle = None
client = Client()

class ApiConnectTestCase(TestCase):
    def setUp(self):
        global sitl_vehicle
        sitl_vehicle = dronekit_sitl.start_default()
        print sitl_vehicle.connection_string()

    def tearDown(self):
        global sitl_vehicle        
        sitl_vehicle.stop()

    def test_post_valid_port(self):
        """Submits POST with valid data"""
        response = client.post('/dkconnect/connect/', {'port': 5760,})

        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json["status"], "ok")
        self.assertEqual(response_json["connected"], True)

class ApiConnectTestCaseNoVehicle(TestCase):
    def test_post_valid_port_no_vehicle(self):
        """Submits POST with valid data, but doesn't start the vehicle to connect"""
        response = client.post('/dkconnect/connect/', {'port': 5760,})

        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json["status"], "error")
        self.assertEqual(response_json["connected"], False)

