import json
import logging

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Import DroneKit-Python
from dronekit import connect, VehicleMode
vehicle = None

logger = logging.getLogger(__name__)

@require_http_methods(["GET"])
def index(request):
    return render(request, 'dkconnect/index.html', None)

@require_http_methods(["POST"])
def api_connect(request):
    # If instance of vehicle exists, assume needs to disconnect.
    # Otherwise assume needs to start connection
    if not vehicle:
        is_connected, status = _handle_connect(request)
    else:
        is_connected, status = _handle_disconnect(request)

    return JsonResponse({'connected': is_connected, 'status': status})

def _handle_connect(request):
    global vehicle

    vehicle_port = request.POST['port']
    vehicle_ipstring = 'tcp:127.0.0.1:%s' % vehicle_port

    logger.debug("Connecting to vehicle on: %s" % vehicle_ipstring)
    try:
        vehicle = connect(vehicle_ipstring, wait_ready=True)
        logger.debug("connected")
        return (True, "ok")

    except:
        logger.error("Failed to connect to drone")
        return (False, "error")

def _handle_disconnect(request):
    global vehicle

    logger.debug("Closing vehicle")
    vehicle.close()
    vehicle = None

    logger.debug("Closed")
    return (False, "ok")