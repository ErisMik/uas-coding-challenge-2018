from django.conf.urls import url

from dkconnect import views

urlpatterns = [
    url('connect/', views.api_connect, name='connect'),
    url('', views.index, name='index'),
]